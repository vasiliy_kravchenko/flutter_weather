import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'CitiesManager.dart';


Future<Map<String, dynamic>> fetchCurrentWeather(http.Client client) async {
  CitiesManager cm = CitiesManager();
  final response =
  await client.get('https://api.openweathermap.org/data/2.5/weather?id=${await cm.getCurrentCityID()}&units=metric&lang=ru&appid=e118ae44cf1455629dfce7a21870a8c0');
  return compute(parseCurrentWeather, response.body);
}

Map<String, dynamic> parseCurrentWeather(String responseBody) {
  return Map<String, dynamic>.from(json.decode(responseBody));
}

class CurrentWeather extends StatelessWidget {
  final Map<String, dynamic> currWeather;

  CurrentWeather({Key key, this.currWeather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var dateNow = new DateTime.now();
    var format = new DateFormat('HH:mm');
    double temp = double.tryParse(currWeather['main']['temp'].toString());
    double tempMin = double.tryParse(currWeather['main']['temp_min'].toString());
    double tempMax = double.tryParse(currWeather['main']['temp_max'].toString());
    int humidity = int.tryParse(currWeather['main']['humidity'].toString());
    double pressure = double.tryParse(currWeather['main']['pressure'].toString()) / 1.33322387415; //hPa -> mmhg
    String icon = currWeather['weather'][0]['icon'].toString();
    double windSpeed = double.tryParse(currWeather['wind']['speed'].toString());

    CitiesManager cm = CitiesManager();
    cm.setCurrentCityName(currWeather['name'].toString());

    return SingleChildScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      child:Column(
        children: [
          Container(
            padding: new EdgeInsets.all(4.0),
            child: Text('Обновлено: ${format.format(dateNow).toString()}', style: new TextStyle(fontSize: 14.0, color: Colors.black45)),
          ),
          Container(
            padding: new EdgeInsets.all(1.0),
            child: Text('${currWeather['name'].toString()}, ${currWeather['sys']['country'].toString()}',style: new TextStyle(fontSize: 36.0, color: Color.fromRGBO(255, 255, 255, 0.7))),
          ),


          Container(
            padding: new EdgeInsets.only(top:4.0, bottom: 4.0, left: 10.0, right: 10.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0.5), borderRadius: new BorderRadius.only(
                topLeft:  const  Radius.circular(5.0), bottomLeft:  const  Radius.circular(5.0),
                topRight: const  Radius.circular(5.0), bottomRight: const  Radius.circular(5.0),)
              ),
              child:Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Image.network('http://openweathermap.org/img/wn/$icon@2x.png', width: 80.0, height: 80.0, fit: BoxFit.cover,),
                        Container(
                          child: Container(
                            padding: EdgeInsets.only(bottom: 5.0),
                            child: Text(' ${currWeather['weather'][0]['description'].toString()} ', style: new TextStyle(fontSize: 16.0, color: Colors.white)),
                          ),
                        ),
                      ],
                    ),
                  ),


                  Expanded(
                    child: Column(
                      children: [
                        //
                        // Temperature bloc
                        //
                        Container(
                          child: Row(
                            children: [
                              Text('${temp.toStringAsFixed(0)}°C ', style: new TextStyle(fontSize: 42.0, color: Colors.lightGreenAccent)),
                              Column(
                                children:[
                                  Text(' max: ${tempMax.toStringAsFixed(0)}° C', style: new TextStyle(fontSize: 10.0, color: Colors.lightGreenAccent)),
                                  Text(' min: ${tempMin.toStringAsFixed(0)}° C', style: new TextStyle(fontSize: 10.0, color: Colors.lightGreenAccent)),
                                ],
                              ),
                            ],
                          ),
                        ),

                        //
                        //  Humidity bloc
                        //
                        Container(
                          padding: EdgeInsets.only(top: 10.0),
                          child: Text('влажность: $humidity%', style: new TextStyle(fontSize: 14.0, color: Colors.lightGreenAccent)),
                        ),


                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),

          Container(
            padding: new EdgeInsets.all(10.0),
            child: Container(
              decoration: BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0.5), borderRadius: new BorderRadius.only(
                topLeft:  const  Radius.circular(5.0), bottomLeft:  const  Radius.circular(5.0),
                topRight: const  Radius.circular(5.0), bottomRight: const  Radius.circular(5.0),)
              ),
              child: Column(
                children: [

                  //
                  // Wind bloc
                  //

                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.only(left: 80.0),
                          child: Icon(Icons.flag, color: Colors.white60, size: 20.0),
                        ),
                      ),
                      Expanded(
                        child: Text('${windSpeed.toStringAsFixed(0)} м/с', style: new TextStyle(fontSize: 14.0, color: Colors.white60)),
                      ),
                    ],
                  ),

                  //
                  // Pressure bloc
                  //

                  Container(
                    padding: EdgeInsets.only(top: 1.0),
                    child: Text('давление: ${pressure.toStringAsFixed(0)} мм рт.ст.', style: new TextStyle(fontSize: 14.0, color: Colors.white60)),
                  ),
                ],
              ),
            ),
          ),

        ],
      ),
    );

  }

}
