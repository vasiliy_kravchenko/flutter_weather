import 'package:flutter/material.dart';
import 'SelectCity.dart';
import 'CitiesManager.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CitiesManager cm = CitiesManager();
    return Drawer(
      child: Container(
        color: Theme.of(context).backgroundColor,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(
                child: Column(
                  children: [
                    Container(
                      width: 260.0,
                      child: Text('Flutter Weather', style: new TextStyle(fontSize: 26.0, color: Colors.deepPurple)),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 40.0),
                      width: 260.0,
                      child: Text('${cm.getCurrentCityName()}', style: new TextStyle(fontSize: 20.0, color: Colors.white)),
                    ),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/sky.png"),
                  fit: BoxFit.cover,
                ),
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text('Выбрать город'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SelectCity()),
                );
              },
            ),
            Divider(),
            ListTile(
              title: Text('Назад'),
              onTap: () { Navigator.pop(context); },
            ),

          // test
          ],
        ),
      ),
    );
  }

}
