import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'CitiesManager.dart';


class ForecastList extends StatelessWidget {
  final List<Forecast> forecasts;

  ForecastList({Key key, this.forecasts}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const AlwaysScrollableScrollPhysics(),
      itemCount: forecasts.length,
      itemBuilder: (context, index){
        return ForecastWidget(fc: forecasts[index]);
      },
    );
  }
}

class ForecastDailyList extends StatelessWidget {
  final List<Forecast1Day> forecasts;

  ForecastDailyList({Key key, this.forecasts}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const AlwaysScrollableScrollPhysics(),
      itemCount: forecasts.length,
      itemBuilder: (context, index){
        return Forecast1DayWidget(fc1Day: forecasts[index]);
      },
    );
  }
}


class ForecastWidget extends StatelessWidget {
  final Forecast fc;

  const ForecastWidget({Key key, @required this.fc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black12,
      height: 100.0,
      padding: new EdgeInsets.all(5.0),
      child: Column (
        children: [
          Container (
            height: 10.0,
            alignment: Alignment.centerLeft,
            child: Text(fc.dtTxt, style: new TextStyle(fontSize: 10.0, color: Colors.white)),
          ),

          Row(
            children: [
              Image.network('http://openweathermap.org/img/wn/${fc.icon}@2x.png', width: 80.0, height: 80.0, fit: BoxFit.cover,),
              Text(fc.desc),
              Text('T: ${fc.temp.toStringAsFixed(0)} C'),
            ],
          ),
        ],
      ),

    );
  }
}

class Forecast1DayWidget extends StatelessWidget {
  final Forecast1Day fc1Day;

  const Forecast1DayWidget({Key key, @required this.fc1Day}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> lw = [];
    for(Forecast fc in fc1Day.dayForecasts) {
      lw.add(Forecast3HourWidget(fc: fc));
    }

    var format = new DateFormat.MMMMEEEEd("ru");

    return Container(
      padding: new EdgeInsets.only(top: 5.0, bottom: 5.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0.5), borderRadius: new BorderRadius.only(
          topLeft:  const  Radius.circular(5.0), bottomLeft:  const  Radius.circular(5.0),
          topRight: const  Radius.circular(5.0), bottomRight: const  Radius.circular(5.0),)
        ),
        child: Column(
          children: [
            Container(
              padding: new EdgeInsets.only(top: 2.0, bottom: 2.0),
              child: Container(
                child: Text('${format.format(fc1Day.date).toString()}', style: new TextStyle(fontSize: 16.0, color: Colors.white)),
              ),
            ),
            Row(children: lw,),
          ],
        ),
      ),
    );
  }
}

class Forecast3HourWidget extends StatelessWidget {
  final Forecast fc;
  const Forecast3HourWidget({Key key, @required this.fc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var format = new DateFormat('HH:mm');
    var date = DateTime.fromMillisecondsSinceEpoch(1000 * fc.dt);

    return Expanded(
      child: Container(
        padding: EdgeInsets.only(left: 1.0, right: 1.0),
        child: Container(

          decoration: BoxDecoration(color: Color.fromRGBO(0, 255, 255, 0.4), borderRadius: new BorderRadius.only(
            topLeft:  const  Radius.circular(5.0), bottomLeft:  const  Radius.circular(5.0),
            topRight: const  Radius.circular(5.0), bottomRight: const  Radius.circular(5.0),)
          ),
          child: Column (
            children: [
              Container(
                padding: EdgeInsets.only(top: 5.0),
                child: Text('${format.format(date).toString()}', style: new TextStyle(fontSize: 10.0, color: Colors.black45)),
              ),
              Image.network('http://openweathermap.org/img/wn/${fc.icon}@2x.png', width: 42.0, height: 42.0, fit: BoxFit.cover,),
              Container(
                padding: EdgeInsets.only(bottom: 5.0),
                child: Text('${fc.temp.toStringAsFixed(0)}° C', style: new TextStyle(fontSize: 10.0, color: Colors.white)),
              ),
            ],
          ),
        ),
      ),
    );
  }

}


Future<List<Forecast1Day>> fetchForecast(http.Client client) async {
  CitiesManager cm = CitiesManager();
  final response =
  await client.get('http://api.openweathermap.org/data/2.5/forecast?id=${await cm.getCurrentCityID()}&units=metric&lang=ru&appid=e118ae44cf1455629dfce7a21870a8c0');
  return compute(parseForecast, response.body);
}

List<Forecast1Day> parseForecast(String responseBody) {
  final parsed = Map<String, dynamic>.from(json.decode(responseBody));

  // Генерация прогноза на 4 дня от текущей даты
  List<Forecast1Day> forecast4Day = [];
  var dateNow = new DateTime.now();
  for(int i = 1; i < 5; ++i) {
    var forecastDate = dateNow.add(Duration(days: i));
    forecast4Day.add(Forecast1Day.fromJson4Date(forecastDate, parsed));
  }
  return forecast4Day;
}

class Forecast {
  final int dt;
  final double temp;
  final String desc;  // текстовое описание "легкий дождь"
  final String icon;  // иконка погоды
  final double windSpeed;

  final String dtTxt;

  Forecast({this.dt, this.temp, this.desc, this.icon, this.windSpeed, this.dtTxt});

  factory Forecast.fromJson(Map<String, dynamic> json) {
    return Forecast(
      dt: json['dt'] as int,
      temp: double.tryParse(json['main']['temp'].toString()),
      desc: json['weather'][0]['description'] as String,
      icon: json['weather'][0]['icon'] as String,
      windSpeed: double.tryParse(json['wind']['speed'].toString()),
      dtTxt: json['dt_txt'] as String,
    );
  }

}

class Forecast1Day {
  final DateTime date;
  final List<Forecast> dayForecasts;

  Forecast1Day({this.date, this.dayForecasts});

  factory Forecast1Day.fromJson4Date(DateTime dateTime, Map<String, dynamic> json) {
    List<Forecast> fcs = [];
    int day = dateTime.day;
    for (Map<String, dynamic> fc3Hour in json['list'] as List) {
      if (day == DateTime.fromMillisecondsSinceEpoch(1000 * (fc3Hour['dt'] as int)).day) {
        fcs.add(Forecast.fromJson(fc3Hour));
      }
    }
    return Forecast1Day(date: dateTime, dayForecasts: fcs);
  }

}

