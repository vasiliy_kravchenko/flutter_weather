import 'package:shared_preferences/shared_preferences.dart';

const String defaultCity = '524901'; //Moscow

class CitiesManager {
  static final CitiesManager _singleton = new CitiesManager._internal();
  String _currentCity;
  String _currentCityName = '';

  factory CitiesManager() {
    return _singleton;
  }

  Future<String> getCurrentCityID() async {
    if (_currentCity == null || _currentCity.isEmpty) {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      String currCity = prefs.getString('currentCity');
      if (currCity != null && currCity.isNotEmpty) {
        _currentCity = currCity;
      } else {
        prefs.setString('currentCity', defaultCity);
        _currentCity = defaultCity;
      }
    }

    return _currentCity;
  }

  Future<bool> setCurrentCityID(String currentCityID) async {
    _currentCity = currentCityID;
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString('currentCity', _currentCity);
  }

  String getCurrentCityName() {
    return _currentCityName;
  }

  void setCurrentCityName(String name) {
    _currentCityName = name;
  }

  CitiesManager._internal();



}