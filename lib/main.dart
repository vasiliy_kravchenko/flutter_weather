import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'forecast.dart';
import 'myDrawer.dart';
import 'CurrentWeather.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Weather',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: MyHomePage(title: 'Flutter Weather'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text(widget.title),),
      drawer: MyDrawer(),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/akkem.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Container (
          child: Center (
            child: RefreshIndicator(
              onRefresh: _refreshForecast,
              child: Container(

                child: Column(

                  children: [
                    Expanded(
                      child:FutureBuilder<Map<String, dynamic>>(
                        future: fetchCurrentWeather(http.Client()),
                        builder: (context, snapshot){
                          if (snapshot.hasError) print(snapshot.error);
                          return snapshot.hasData ? CurrentWeather(currWeather: snapshot.data) : Center(child: CircularProgressIndicator());
                        }
                      )
                    ),

                    Expanded(
                      child: FutureBuilder<List<Forecast1Day>>(
                        future: fetchForecast(http.Client()),
                        builder: (context, snapshot) {
                          if (snapshot.hasError) print(snapshot.error);
                          return snapshot.hasData ? ForecastDailyList(forecasts: snapshot.data) : Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),

                  ],

                ),

              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    initializeDateFormatting();
  }

  Future<void> _refreshForecast() async {
    setState(() {});
  }
}


