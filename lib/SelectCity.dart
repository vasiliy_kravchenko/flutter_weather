import 'package:flutter/material.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'CitiesManager.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';


class SelectCity extends StatefulWidget {
  SelectCity({Key key}) : super(key: key);

  @override
  _SelectCityState createState() => _SelectCityState();

}

class _SelectCityState extends State<SelectCity> {
  String _searchVal;
  String _country = 'RU';
  static final  validCharacters = RegExp(r'^[a-zA-Z0-9\s-]+$');
  final formKey = new GlobalKey<FormState>();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text('Выбор города'),),
      body: Container(

        child: Column(
          children: [

            Container(
              child :Form(
                key: formKey,
                child: Column(
                  children: [

                    Container(
                      child: TextFormField(
                        decoration: InputDecoration(labelText: "Город"),
                        style: TextStyle(fontSize: 20.0, color: Colors.black45),
                        onSaved: (val) { _searchVal = val;},
                        validator: (val) {
                            return val.length < 3 || !validCharacters.hasMatch(val) ? 'Название города по английски, не менее 3 букв' : null;
                          },
                      ),
                      width: 400.0,
                    ),

                    CountryCodePicker(
                      onChanged: (val) => _country = val.code,//countryChanged,
                      initialSelection: 'RU',
                      favorite: ['RU', 'US'],
                      showCountryOnly: true,
                      showOnlyCountryWhenClosed: true,
                      alignLeft: false,
                    ),

                    MaterialButton(
                      color: Theme.of(context).accentColor,
                      child: Text('искать'),
                      onPressed: searchPressed,
                    ),

                  ],
                ),
              ),
            ),

            Flexible(
              child: FutureBuilder<List<FoundedCity>>(
                future: searchCity(http.Client(), _searchVal, _country),
                builder: (context, snapshot) {
                  if (snapshot.hasError) print(snapshot.error);
                  if (_searchVal == null || _searchVal.isEmpty)
                    return Center(child: Text(''));
                  else if (snapshot.hasData && !isLoading)
                    return FoundedCityList(cities: snapshot.data,);
                  else
                    return Center(child: CircularProgressIndicator());
                },
              ),
            ),

          ],
        ),
      ),
    );
  }

  void searchPressed() {
    final form = formKey.currentState;
    if (form.validate()) {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      form.save();
      isLoading = true;
      setState(() {});
    }
  }

  Future<List<FoundedCity>> searchCity(http.Client client, String city, String country) async {
    if (city == null || city.isEmpty) return null;
    final response =
    await client.get('http://api.openweathermap.org/data/2.5/find?q=$city,$country&units=metric&lang=ru&appid=e118ae44cf1455629dfce7a21870a8c0');
    isLoading = false;
    return compute(parseQuery, response.body);
  }

}

class FoundedCityList extends StatelessWidget {
  final List<FoundedCity> cities;

  FoundedCityList({Key key, this.cities}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const AlwaysScrollableScrollPhysics(),
      itemCount: cities.length,
      shrinkWrap : true,
      scrollDirection: Axis.vertical,
      itemBuilder: (context, index){
        return FoundedCityWidget(city: cities[index]);
      },
    );
  }
}



List<FoundedCity> parseQuery(String responseBody) {
  final parsed = Map<String, dynamic>.from(json.decode(responseBody));
  List<FoundedCity> res = [];
  if (int.tryParse(parsed['count'].toString()) > 0) {
    for (Map<String, dynamic> city in parsed['list'] as List) {
      res.add(FoundedCity.fromJson(city));
    }
  } else {
    res.add(FoundedCity.notFound());
  }

  return res;
}



class FoundedCityWidget extends StatelessWidget {
  final FoundedCity city;
  const FoundedCityWidget({Key key, @required this.city}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    if (city.id.isEmpty) {
      return Container(
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: Text('Нет совпадений', style: new TextStyle(fontSize: 24.0, color: Colors.black54)),
        ),
      );
    }

    return Container(
      padding: EdgeInsets.all(10.0),
      child: Row(
        children: [
          MaterialButton(
            color: Theme.of(context).accentColor,
            child: Text('Выбрать'),
            onPressed: (){
              CitiesManager cm = CitiesManager();
              cm.setCurrentCityID(city.id);
              cm.setCurrentCityName(city.name);
              Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));},
          ),

          Container(
            padding: EdgeInsets.only(left: 30.0),
            child: Column(
              children: [
                Text('${city.name}, ${city.country}', style: new TextStyle(fontSize: 24.0, color: Colors.black54)),
                Text('широта: ${city.lat}, долгота: ${city.lon}',style: new TextStyle(fontSize: 10.0, color: Colors.black54)),
              ],
            ),
          ),
        ],
      ),

    );
  }


}


class FoundedCity {
  final String id;
  final String name;
  final String country;
  final String lat;
  final String lon;
  FoundedCity({this.id, this.name, this.country, this.lat, this.lon});

  factory FoundedCity.fromJson(Map<String, dynamic> json) {
    return FoundedCity(
      id: json['id'].toString(),
      name: json['name'].toString(),
      country: json['sys']['country'].toString(),
      lat: json['coord']['lat'].toString(),
      lon: json['coord']['lon'].toString(),
    );
  }

  factory FoundedCity.notFound() {
    return FoundedCity(id: '', name: '', country: '', lat: '', lon:  '');
  }

}